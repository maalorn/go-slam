package main

import (
	"os"
	"log"
	"image"
	_ "image/jpeg"
	"image/color"
	"math/rand"
	"time"
	"image/gif"
	"image/jpeg"
)

func main() {
	reader, err := os.Open("map1.jpeg")
	if err != nil {
		log.Fatalln(err)
	}
	defer reader.Close()

	im, _, err := image.Decode(reader)
	if err != nil {
		log.Fatalln(err)
	}

	bounds := im.Bounds()

	anim := &gif.GIF{}

	var palette color.Palette = color.Palette{}
	c := color.Gray{0xff / 2}
	log.Printf("c: %#v\n", c)
	palette = append(palette, color.White)
	palette = append(palette, color.Black)
	var i uint8
	for i = 0; i < 254; i++ {
		palette = append(palette, color.Gray{i})
	}

	rec := image.Rect(0,0,640,480)
	guess := image.NewPaletted(rec, palette)
	guessBounds := guess.Bounds()

	for y := guessBounds.Min.Y; y < guessBounds.Max.Y; y++ {
		for x := guessBounds.Min.X; x < guessBounds.Max.X; x++ {
			guess.Set(x,y,c)
		}
	}

	images := make([]*image.Paletted, 0)
	delays := make([]int, 0)

	start := time.Now()
	for i := 0; i < 100000; i++ {
		x, y := rand.Intn(bounds.Max.X), rand.Intn(bounds.Max.Y)
		if x < bounds.Min.X || y < bounds.Min.Y {
			continue
		}
		r,_,_,_ := im.At(x,y).RGBA()
		rr, _, _, _ := guess.At(x,y).RGBA()

		var v uint8
		var px uint8 = uint8(rr / 0xff)
		if r != 0 {
			v = px + 50
			if v < px {
				v = 254
			}
		} else {
			v = px - 50
			if v > px {
				v = 0
			}
		}

		c := color.RGBA{R:v, G:v, B:v, A:255}
		guess.Set(x, y, c)

		if i % 1000 == 0 {
			curr := image.NewPaletted(rec, palette)
			copy(curr.Pix, guess.Pix)
			images = append(images, curr)
			delays = append(delays, 10)
		}
	}
	anim.Image = images
	anim.Delay = delays
	anim.LoopCount = 1
	log.Printf("Dart throwing took: %s\n", time.Now().Sub(start))
	log.Printf("Got %d frames and %d delays.\n", len(anim.Image), len(anim.Delay))

	writer, err := os.Create("guessanim.gif")
	if err != nil {
		log.Fatalln(err)
	}
	defer writer.Close()

	avg := image.NewPaletted(rec, palette)
	for y := guessBounds.Min.Y + 1; y < guessBounds.Max.Y - 1; y++ {
		for x := guessBounds.Min.X + 1; x < guessBounds.Max.X + 1; x++ {
			topLeft, _, _, _ := guess.At(x-1, y-1).RGBA()
			topCenter, _, _, _ := guess.At(x, y-1).RGBA()
			topRight, _, _, _ := guess.At(x+1, y-1).RGBA()
			centerLeft, _, _, _ := guess.At(x-1, y).RGBA()
			centerRight, _, _, _ := guess.At(x+1, y).RGBA()
			bottomLeft, _, _, _ := guess.At(x-1, y+1).RGBA()
			bottomCenter, _, _, _ := guess.At(x, y+1).RGBA()
			bottomRight, _, _, _ := guess.At(x+1, y+1).RGBA()
			center, _, _, _ := guess.At(x, y).RGBA()
			var a float64 = (
				float64(topLeft) + float64(topCenter) + float64(topRight) + float64(centerLeft) +
				float64(centerRight) + float64(bottomLeft) + float64(bottomCenter) + float64(bottomRight) + float64(center) ) / 9.0 / 0xff
			if a >= 128 {
				a = 0xff
			} else {
				a = 0
			}
			avg.Set(x, y, color.RGBA{uint8(a), uint8(a), uint8(a), 0xff})
		}
	}

	avg2 := image.NewPaletted(rec, palette)
	for y := guessBounds.Min.Y + 1; y < guessBounds.Max.Y - 1; y++ {
		for x := guessBounds.Min.X + 1; x < guessBounds.Max.X + 1; x++ {
			topLeft, _, _, _ := avg.At(x-1, y-1).RGBA()
			topCenter, _, _, _ := avg.At(x, y-1).RGBA()
			topRight, _, _, _ := avg.At(x+1, y-1).RGBA()
			centerLeft, _, _, _ := avg.At(x-1, y).RGBA()
			centerRight, _, _, _ := avg.At(x+1, y).RGBA()
			bottomLeft, _, _, _ := avg.At(x-1, y+1).RGBA()
			bottomCenter, _, _, _ := avg.At(x, y+1).RGBA()
			bottomRight, _, _, _ := avg.At(x+1, y+1).RGBA()
			center, _, _, _ := avg.At(x, y).RGBA()
			var a float64 = (
			float64(topLeft) + float64(topCenter) + float64(topRight) + float64(centerLeft) +
			float64(centerRight) + float64(bottomLeft) + float64(bottomCenter) + float64(bottomRight) + float64(center) ) / 9.0 / 0xff
			if a >= 128 {
				a = 0xff
			} else {
				a = 0
			}
			avg2.Set(x, y, color.RGBA{uint8(a), uint8(a), uint8(a), 0xff})
		}
	}

	imwriter, err := os.Create("guess1.jpeg")
	if err != nil {
		log.Fatalln(err)
	}
	defer imwriter.Close()
	if err := jpeg.Encode(imwriter, avg, &jpeg.Options{Quality:100}); err != nil {
		log.Fatalln(err)
	}

	imwriter2, err := os.Create("guess2.jpeg")
	if err != nil {
		log.Fatalln(err)
	}
	defer imwriter2.Close()
	if err := jpeg.Encode(imwriter2, avg2, &jpeg.Options{Quality:100}); err != nil {
		log.Fatalln(err)
	}

	imwriter3, err := os.Create("guess0.jpeg")
	if err != nil {
		log.Fatalln(err)
	}
	defer imwriter3.Close()
	if err := jpeg.Encode(imwriter3, images[len(images)-1], &jpeg.Options{Quality:100}); err != nil {
		log.Fatalln(err)
	}

	log.Println("Encoding gif...")
	if err := gif.EncodeAll(writer, anim); err != nil {
		log.Fatalln(err)
	}
	log.Println("Done.")
}